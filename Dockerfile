FROM debian:sid

ARG USER_NAME=latex
ARG USER_HOME=/home/latex
ARG USER_ID=1000
ARG USER_GECOS=LaTeX

RUN useradd \
  --home-dir "$USER_HOME" \
  --uid $USER_ID \
  --gecos "$USER_GECOS" \
  --no-log-init \
  --disabled-password \
  "$USER_NAME"

ARG WGET=wget
ARG GIT=git
ARG MAKE=make
ARG PYGMENTS=python3-pygments
ARG MATPLOTLIB=python3-matplotlib
ARG SCIPY=python3-scipy
ARG SYMPY=python3-sympy
ARG PRETTYTABLE=python3-prettytable

RUN apt update && \
  apt dist-upgrade -y && \
  apt install --without-install-recommends -y \
  bash \
  python3-certifi \
  python3-requests \
  python3-pip && \
  chsh -s /bin/bash latex
RUN apt install --without-install-recommends -y \
  texlive \
  texlive-base \
  texlive-bibtex-extra \
  texlive-binaries \
  texlive-extra-utils \
  texlive-font-utils \
  texlive-fonts-extra \
  texlive-fonts-extra-links \
  texlive-fonts-recommended \
  texlive-formats-extra \
  texlive-lang-french \
  texlive-latex-base \
  texlive-latex-extra \
  texlive-latex-recommended \
  texlive-pstricks \
  texlive-pictures \
  texlive-plain-generic \
  texlive-science \
  texlive-xetex
RUN apt install --without-install-recommends -y \
  latexmk \
  rubber \
  lmodern \
# some auxiliary tools
  "$WGET" \
  curl \
  "$GIT" \
  "$MAKE" \
  # syntax highlighting package
  "$PYGMENTS" \
  # pythontex specific
  "$SCIPY" "$SYMPY" "$PRETTYTABLE"
  # Remove more unnecessary stuff
RUN apt remove --purge -y \*-doc && \
  apt autoremove -y  && \
  apt autoclean -y

USER "$USER_NAME"
